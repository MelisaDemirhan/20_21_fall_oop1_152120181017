#include <iostream>
#include <sstream>
using namespace std;

class Student {
private:
    int scores[5];
public:
    void input() {
        for (int i = 0; i < 5; i++)
        {
            cin >> scores[i]; //ogrencilerin notlari giriliyor
        }
    };
    int calculateTotalScore() {
        int sum = 0;
        for (int i = 0; i < 5; i++)
        {
            sum += scores[i]; //tum notlar toplaniyor
        }
        return sum;
    };
};

int main() {
    int n, kristen_score, count = 0, total;

    cin >> n; //ogrencilerin sayisi

    Student* s = new Student[n]; //alinan n degeri kadar Student class'�nda s adinda dinamik array olusturuluyor

    for (int i = 0; i < n; i++)
    {
        s[i].input(); // n degeri kadar classa gidip deger girisi yapiliyor
    }

    //kristen'in notlarinin ilk notlar oldugu belirleniyor
    //tum notlarin toplanmasi icin class fonksiyonu kullaniliyor
    kristen_score = s[0].calculateTotalScore(); 
    
    for (int i = 1; i < n; i++)
    {
        total = s[i].calculateTotalScore(); //sirasi ile diger ogrencilerin notlari toplaniyor
        if (total > kristen_score) //herhangi birinin notlar� kristendan yuksekse bu if condition'� cal�s�yor
        {
            count++;//count degeri 1 artt�r�l�yor
        }
    }

    cout << count;

    return 0;
}