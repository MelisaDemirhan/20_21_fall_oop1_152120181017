#include<iostream>
#include<stdlib.h>

using namespace std;

int main()
{
    long long n; //degisken
    cin >> n; // n degiskeni kullanicidan aliniyor
    cin.ignore(n > sizeof(long long), '\n');  // n'in boyutu long long'dan b�y�kse veya '\n' gelmisse umursam�yor

    if (n < 1) //n degeri 1'den kucukse bu if condition'i kullaniliyor. 
    {
        cout << "Lower than 1"; 
    }
    else if (n > 9) //n degeri 9'dan b�y�kse bu if condition'i kullaniliyor.
    {
        cout << "Greater than 9";
    }
    else //n degerinin 1 ve 9 arasinda oldugu durumlar
    {
        // n degeri kac ise ona karsilik gelen rakam harflerle yaziliyor.
        switch (n) {
        case 1:
            cout << "one";
            break;
        case 2:
            cout << "two";
            break;
        case 3:
            cout << "three";
            break;
        case 4:
            cout << "four";
            break;
        case 5:
            cout << "five";
            break;
        case 6:
            cout << "six";
            break;
        case 7:
            cout << "seven";
            break;
        case 8:
            cout << "eight";
            break;
        case 9:
            cout << "nine";
            break;
        default:
            cout << "ERROR";
            break;
        }

    }

    return 0;
}
