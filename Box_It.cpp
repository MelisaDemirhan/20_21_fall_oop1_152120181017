#include <iostream>
#include <sstream>

using namespace std;

class Box {
private:
	int l, b, h; //degiskenler
public:
	Box() 
	{
		l = 0, b = 0, h = 0; //degiskenler sifirlaniyor
	}
	Box(int length, int breadth, int height) 
	{
		l = length, b = breadth, h = height; //classdaki degiskenlere mainden gelenler ataniyor
	}
	Box(const Box& B) 
	{
		l = B.l;
		b = B.b;
		h = B.h;
	}
	int getLenght() 
	{
		return l; //lenght return ediliyor
	}
	int getBreadth() 
	{
		return b; //breadth return ediliyor
	}
	int getHeight() 
	{
		return h; //height return ediliyor
	}
	long long CalculateVolume() 
	{
		long long volume =(long long) l * b * h; //volume degeri degiskenlerin carpimina esit
		return volume; //volume return ediliyor
	}
	friend bool operator < (Box& A, Box& B) 
	{
		if (((A.b < B.b) && (A.l == B.l)) || (A.l < B.l) || ((A.h < B.h) && (A.l == B.l) && (A.b == B.b)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	friend ostream& operator<< (ostream& output, const Box& B)
	{
		output << B.l << " " << B.b << " " << B.h;
		return output;
	}
};

void check2()
{
	int n;
	cin >> n;
	Box temp; //temp nesnesi olusturuluyor
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h; //length, breadth, height degerleri kullanicidan aliniyor
			Box NewBox(l, b, h); //class'a gonderiliyor
			temp = NewBox; 
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h; //length, breadth, height degerleri kullanicidan aliniyor
			Box NewBox(l, b, h); //class'a gonderiliyor
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;//volume degeri i�in class'daki fonksiyon kullaniliyor
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();// check2 fonksiyonu calistiriliyor
}