﻿
namespace OOP2_Lab01
{
    partial class windowCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(windowCalc));
            this.btnAddition = new System.Windows.Forms.Button();
            this.btnSubtraction = new System.Windows.Forms.Button();
            this.btnMultiplication = new System.Windows.Forms.Button();
            this.btnDivision = new System.Windows.Forms.Button();
            this.textBox_Num1 = new System.Windows.Forms.TextBox();
            this.textBox_Num2 = new System.Windows.Forms.TextBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sembol = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ExitButtonCalc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddition
            // 
            this.btnAddition.Location = new System.Drawing.Point(224, 220);
            this.btnAddition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddition.Name = "btnAddition";
            this.btnAddition.Size = new System.Drawing.Size(128, 41);
            this.btnAddition.TabIndex = 0;
            this.btnAddition.Text = "Addition";
            this.btnAddition.UseVisualStyleBackColor = true;
            this.btnAddition.Click += new System.EventHandler(this.btnAddition_Click);
            this.btnAddition.MouseLeave += new System.EventHandler(this.btnAddition_MouseLeave);
            this.btnAddition.MouseHover += new System.EventHandler(this.btnAddition_MouseHover);
            // 
            // btnSubtraction
            // 
            this.btnSubtraction.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSubtraction.Location = new System.Drawing.Point(430, 220);
            this.btnSubtraction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSubtraction.Name = "btnSubtraction";
            this.btnSubtraction.Size = new System.Drawing.Size(128, 41);
            this.btnSubtraction.TabIndex = 1;
            this.btnSubtraction.Text = "Subtraction";
            this.btnSubtraction.UseVisualStyleBackColor = true;
            this.btnSubtraction.Click += new System.EventHandler(this.btnSubtraction_Click);
            this.btnSubtraction.MouseLeave += new System.EventHandler(this.btnSubtraction_MouseLeave);
            this.btnSubtraction.MouseHover += new System.EventHandler(this.btnSubtraction_MouseHover);
            // 
            // btnMultiplication
            // 
            this.btnMultiplication.Location = new System.Drawing.Point(224, 298);
            this.btnMultiplication.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMultiplication.Name = "btnMultiplication";
            this.btnMultiplication.Size = new System.Drawing.Size(128, 41);
            this.btnMultiplication.TabIndex = 2;
            this.btnMultiplication.Text = "Multiplication";
            this.btnMultiplication.UseVisualStyleBackColor = true;
            this.btnMultiplication.Click += new System.EventHandler(this.btnMultiplication_Click);
            this.btnMultiplication.MouseLeave += new System.EventHandler(this.btnMultiplication_MouseLeave);
            this.btnMultiplication.MouseHover += new System.EventHandler(this.btnMultiplication_MouseHover);
            // 
            // btnDivision
            // 
            this.btnDivision.Location = new System.Drawing.Point(430, 298);
            this.btnDivision.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDivision.Name = "btnDivision";
            this.btnDivision.Size = new System.Drawing.Size(128, 41);
            this.btnDivision.TabIndex = 3;
            this.btnDivision.Text = "Division";
            this.btnDivision.UseVisualStyleBackColor = true;
            this.btnDivision.Click += new System.EventHandler(this.btnDivision_Click);
            this.btnDivision.MouseLeave += new System.EventHandler(this.btnDivision_MouseLeave);
            this.btnDivision.MouseHover += new System.EventHandler(this.btnDivision_MouseHover);
            // 
            // textBox_Num1
            // 
            this.textBox_Num1.BackColor = System.Drawing.Color.Silver;
            this.textBox_Num1.Location = new System.Drawing.Point(224, 146);
            this.textBox_Num1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_Num1.Name = "textBox_Num1";
            this.textBox_Num1.Size = new System.Drawing.Size(142, 27);
            this.textBox_Num1.TabIndex = 4;
            this.textBox_Num1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_Num1_KeyDown);
            this.textBox_Num1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Num1_KeyPress);
            // 
            // textBox_Num2
            // 
            this.textBox_Num2.BackColor = System.Drawing.Color.Silver;
            this.textBox_Num2.Location = new System.Drawing.Point(423, 146);
            this.textBox_Num2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_Num2.Name = "textBox_Num2";
            this.textBox_Num2.Size = new System.Drawing.Size(142, 27);
            this.textBox_Num2.TabIndex = 5;
            this.textBox_Num2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_Num2_KeyDown);
            this.textBox_Num2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Num2_KeyPress);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.Transparent;
            this.lblResult.Location = new System.Drawing.Point(660, 145);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 20);
            this.lblResult.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(610, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "=";
            // 
            // sembol
            // 
            this.sembol.AutoSize = true;
            this.sembol.BackColor = System.Drawing.Color.Transparent;
            this.sembol.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.sembol.Location = new System.Drawing.Point(387, 145);
            this.sembol.Name = "sembol";
            this.sembol.Size = new System.Drawing.Size(0, 20);
            this.sembol.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label2.Location = new System.Drawing.Point(245, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Number 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(453, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Number 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label4.Location = new System.Drawing.Point(660, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Result";
            // 
            // ExitButtonCalc
            // 
            this.ExitButtonCalc.BackColor = System.Drawing.Color.Transparent;
            this.ExitButtonCalc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ExitButtonCalc.BackgroundImage")));
            this.ExitButtonCalc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButtonCalc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ExitButtonCalc.ForeColor = System.Drawing.Color.Transparent;
            this.ExitButtonCalc.Location = new System.Drawing.Point(684, 13);
            this.ExitButtonCalc.Name = "ExitButtonCalc";
            this.ExitButtonCalc.Size = new System.Drawing.Size(31, 31);
            this.ExitButtonCalc.TabIndex = 12;
            this.ExitButtonCalc.UseVisualStyleBackColor = false;
            this.ExitButtonCalc.Click += new System.EventHandler(this.ExitButtonCalc_Click);
            // 
            // windowCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(742, 479);
            this.Controls.Add(this.ExitButtonCalc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sembol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.textBox_Num2);
            this.Controls.Add(this.textBox_Num1);
            this.Controls.Add(this.btnDivision);
            this.Controls.Add(this.btnMultiplication);
            this.Controls.Add(this.btnSubtraction);
            this.Controls.Add(this.btnAddition);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "windowCalc";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "windowCalc";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddition;
        private System.Windows.Forms.Button btnSubtraction;
        private System.Windows.Forms.Button btnMultiplication;
        private System.Windows.Forms.Button btnDivision;
        private System.Windows.Forms.TextBox textBox_Num1;
        private System.Windows.Forms.TextBox textBox_Num2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label sembol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ExitButtonCalc;
    }
}

