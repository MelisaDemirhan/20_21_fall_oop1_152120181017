#include <iostream>
#include <string>
#include <sstream>
#include <exception>
using namespace std;

class BadLengthException {
private:
	int x;
public:
	BadLengthException(int Count_Error) {
		x = Count_Error; //username'in uzunlugu x'e atan�yor
	}

	int what() {
		return x; //x return ediliyor
	}
};

bool checkUsername(string username) {
	bool isValid = true;
	int n = username.length(); //username'in uzunlugu hesaplaniyor
	if (n < 5) // 5'ten kucukse BadLengthException class�ndaki ayn� isimli fonksiyona gidiyor
	{
		throw BadLengthException(n);
	}
	for (int i = 0; i < n - 1; i++) {
		if (username[i] == 'w' && username[i + 1] == 'w') //arka arkaya 'w' gelip gelmedigine bakiliyor
		{
			isValid = false; //geliyorsa false deger aliyor. gelmiyorsa true degerde kaliyor
		}
	}
	return isValid;
}

int main() {
	int T; 
	cin >> T; //kac tane string alinacag� giriliyor
	while (T--) {
		string username;
		cin >> username; //stringler giriliyor
		try {
			bool isValid = checkUsername(username); //username kontrol icin fonksiyona gonderiliyor
			if (isValid) //gelen deger true ise
			{
				cout << "Valid" << '\n';
			}
			else //gelen deger false ise
			{
				cout << "Invalid" << '\n';
			}
		}
		catch (BadLengthException e) {
			cout << "Too short: " << e.what() << '\n'; //class'daki what fonksiyonu calisiyor. what fonksiyonundan x degeri return ediyor.
			//x degeri de username'in uzunlugu
		}
	}
	return 0;
}