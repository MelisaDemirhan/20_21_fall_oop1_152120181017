#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>

using namespace std;

int Sum(int*, int);
long long Product(int*, int);
float Average(int*, int);
int Smallest(int*, int);


/**
* main program acildiginda ilk calisan fonksiyondur.\n
* Kullanicidan input dosyasinin ismi istenir.\n
* Eger dosya acilamiyorsa hata verir.\n
* Dosyadaki ilk deger boyut degiskenine atanir. Boyut degeri boyutunda A dizisi dinamik olarak olusturulur.\n
* Geri kalan degerler A dizisine atilir.\n
* boyut ve diziye gelen eleman sayisi ayni degilse hata verir.\n
* @see Sum(int* A, int b)
* Dizideki elemanlarin toplam degeri bulunmasi icin Sum fonskiyonu cagrilir. Gelen deger ekrana bastirilir.\n
* @see Product(int* A, int b)
* Dizideki elemanlarin carpimini bulmak icin Product fonksiyonu cagrilir. Gelen deger ekrana bastirilir.\n
* @see Average(int* A, int b)
* Dizideki elemanlarin ortalamasini bulmak icin Average fonksiyonu cagrilir. Gelen deger ekrana bastirilir.\n
* @see Smallest(int* A, int b)
* Dizideki en kucuk elemani bulmak icin Smallest fonksiyonu cagrilir. Gelen deger ekrana bastirilir.\n
*/
int main() {

	// Degiskenler
	string dosyaAdi;
	int* A;
	int boyut, SumOfNumbers, SmallestOfNumbers;
	long long ProductOfNumbers;
	float AverageOfNumbers;


	// input dosyasinin adi kullanicidan isteniyor.
	cout << "Enter the name of file: ";
	getline(cin, dosyaAdi);

	// input dosyasi okunuyor.
	fstream dosya(dosyaAdi, ios::in);
	if (!dosya)
	{
		cout << "ERROR!";
		exit(0);
	}

	// Ilk verilen deger boyut olarak ataniyor.
	dosya >> boyut;
	if (dosya.eof())
	{
		cout << endl << "ERROR. Dosya bos.";
		exit(0);
	}

	// A dizisinin boyutu dinamik olarak ayarlaniyor.
	A = new int[boyut];
	for (int i = 0; i < boyut; i++)
	{
		// Gelen degerlerin sayisi boyut'tan az olursa hata veriliyor. .eof() ile dosya sonunun gelip gelmedigi kontrol ediliyor.
		if (dosya.eof())
		{
			cout << endl << "ERROR. Verilen degerlerin sayisi boyut'tan az.";
			exit(0);
		}
		// input dosyasindaki diger degerler A dizisine eleman olarak ataniyor.
		dosya >> A[i];
	}

	// Gelen degerlerin sayisi boyut'tan fazla olursa hata veriliyor. .eof() ile dosya sonunun gelip gelmedigi kontrol ediliyor.
	if (!dosya.eof())
	{
		cout << endl << "ERROR. Verilen degerlerin sayisi boyut'tan fazla.";
		exit(0);
	}

	// Dizideki elemanlarin toplamini bulmak icin Sum fonksiyonu kullaniliyor.
	SumOfNumbers = Sum(A, boyut);
	// Sum degeri ekrana bastiriliyor.
	cout << "Sum is " << SumOfNumbers << endl;

	// Dizideki elemanlarin carpimini bulmak icin Product fonksiyonu kullaniliyor.
	ProductOfNumbers = Product(A, boyut);
	// Product degeri ekrana bastiriliyor.
	cout << "Product is " << ProductOfNumbers << endl;

	// Dizideki elemanlarin ortalamasini bulmak icin Average fonksiyonu kullaniliyor.
	AverageOfNumbers = Average(A, boyut);
	// Average degeri ekrana bastiriliyor.
	cout << "Average is " << AverageOfNumbers << endl;

	// Dizideki en k�c�k elemani bulmak icin Smallest fonksiyonu kullaniliyor.
	SmallestOfNumbers = Smallest(A, boyut);
	// Smallest degeri ekrana bastiriliyor
	cout << "Smallest is " << SmallestOfNumbers << endl;

	dosya.close();
	delete[] A;
	system("pause");
}
/**
* Fonksiyon dizideki tum elemanlarin toplam degerini hesapliyor
* @param A pointer integer degisken
* @param b integer degisken
* @returns sum
*/
int Sum(int* A, int b) {
	int sum = 0;

	for (int i = 0; i < b; i++)
	{
		sum += A[i];
	}
	return sum;
}
/**
* Fonksiyon dizideki tum elemanlarin carpim degerini hesapliyor
* @param A pointer integer degisken
* @param b integer degisken
* @returns product
*/
long long Product(int* A, int b) {
	long long product = 1;

	for (int i = 0; i < b; i++)
	{
		product *= A[i];
	}
	return product;
}
/**
* Fonksiyon dizideki tum elemanlarin ortalama degerini hesapliyor
* @param A pointer integer degisken
* @param b integer degisken
* @see Sum(int* A, int b)
* @returns average
*/
float Average(int* A, int b) {
	float average;

	average = (float)Sum(A, b) / (float)b;
	return average;
}
/**
* Fonksiyon dizideki tum elemanlarin arasindan en kucuk olani buluyor
* @param A pointer integer degisken
* @param b integer degisken
* @returns smallest
*/
int Smallest(int* A, int b) {
	int smallest;

	smallest = A[0]; // smallest olarak ilk eleman atan�yor.
	for (int i = 0; i < b; i++)
	{
		smallest = smallest > A[i] ? A[i] : smallest; // Yeni eleman smallest'tan daha k���kse smallest degisiyor degilse ayni kaliyor.
	}
	return smallest;
}