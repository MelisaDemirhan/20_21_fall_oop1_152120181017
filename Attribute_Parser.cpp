#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int main() {
    vector<vector<string> > A;
    string first = ""; //baslangic olarak "" aliniyor
    int n, q;

    cin >> n >> q; //hrlm'deki line sayilari

    for (int i = 0; i < n; ++i)
    {
        string str;
        cin >> str; //string kullanicidan aliniyor

        if (str[0] == '<' && str[1] == '/') //string'in "</" ile baslayip baslamadigina bakiliyor
        {
            string tagname = str.substr(2, str.size() - 3);

            if (!(first.compare(tagname)))//first ve tagname'in ayni olup olmadigina bakiliyor
            {
                first = "";
            }
            else
            {
                first = first.substr(0, first.size() - tagname.size() - 1); 
            }

            continue;
        }

        if (str[str.size() - 1] == '>') //string'in sonunun > olup olmadigina bakiliyor
        {
            if (first.compare(""))// first'un "" ile ayni olup olmadigina bakiliyor
            {
                first += ".";
            } 
            first += str.substr(1, str.size() - 2);

            continue;
        }

        if (first.compare("")) // first'un "" ile ayni olup olmadigina bakiliyor
        {
            first += ".";
        }
        first += str.substr(1, str.size() - 1);
       

        vector<string> temp(1, first);
        string str2;
        cin >> str2;

        while (str2[str2.size() - 1] != '>') //str2'nin sonu > olmadigi surece devame ediyor
        {
            if (!(str2.compare("="))) // str2'nin "=" ile ayni olup olmadigina bakiliyor
            {
                cin >> str2;
                continue;
            }

            temp.push_back(str2);
            cin >> str2;
        }

        temp.push_back(str2.substr(0, str2.size() - 1));
        A.push_back(temp);
    }

    string str3;
    for (int i = 0; i < q; ++i)
    {
        cin >> str3;

        int firstV = str3.find_first_of('~'); //str3'teki ilk '~' elemaninin yeri firstV degiskenine ataniyor

        string key = str3.substr(0, firstV);
        string Attribute = str3.substr(firstV + 1);

        int control = 0;
        for (int j = 0; j < A.size(); ++j)
        {
            if ((!A[j][0].compare(key))) // A[j][0]'in key ile ayni olup olmadigina bakiliyor
            {
                for (int k = 1; k < A[j].size() - 1; k += 2)
                {
                    if (!(A[j][k].compare(Attribute))) // A[j][k]'nin Attribute ile ayni olup olmadigina bakiliyor
                    {
                        control = 1;
                        cout << A[j][k + 1].substr(1, (A[j][k + 1].size()) - 2) << endl;

                        break;
                    }
                }
                if (control) //control degeri 1 ise
                {
                    break;
                }
                   
            }
        }
        if (!control) //control degeri 0 ise
        {
            cout << "Not Found!" << endl;
        }
            
    }
    return 0;
}