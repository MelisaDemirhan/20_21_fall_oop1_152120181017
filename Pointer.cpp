#include <iostream>
using namespace std;

void update(int*, int*);

int main() {
    //degiskenler
    int a, b; 
    int* pa = &a, * pb = &b; //*pa ve *pb'ye a ve b'nin adresleri ataniyor

    cin >> a >> b; //a ve b degerleri kullanicidan aliniyor
    update(pa, pb); //pa ve pb fonksiyona gidiyor
    cout << a << endl << b;

    return 0;
}
void update(int* pa, int* pb) {
    int c = *pa, d = *pb;

    *pa = c + d;
    *pb = c - d;

    if (*pb < 0) 
    {
        *pb = -1 * (*pb); //farklar� negatif sayi geliyorsa mutlak degeri aliniyor
    }
}