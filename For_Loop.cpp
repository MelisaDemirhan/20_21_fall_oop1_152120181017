#include <iostream>
#include <cstdio>
using namespace std;

int main() {

    int a, b; //degiskenler

    cin >> a >> b; //degiskenler kullanicidan aliniyor

    for (int i = a; i <= b; i++) //a ve b degiskenleri arasindaki tum say�lar tek tek aliniyor
    {
        if ((i >= 1) && (i <= 9)) //alinan sayi 1 ve 9 araliginda ise bu if condition'i kullaniliyor
        {
            // alinan sayi kac ise ona karsilik gelen rakam harflerle yaziliyor.
            switch (i) {
            case 1:
                cout << "one" << endl;
                break;
            case 2:
                cout << "two" << endl;
                break;
            case 3:
                cout << "three" << endl;
                break;
            case 4:
                cout << "four" << endl;
                break;
            case 5:
                cout << "five" << endl;
                break;
            case 6:
                cout << "six" << endl;
                break;
            case 7:
                cout << "seven" << endl;
                break;
            case 8:
                cout << "eight" << endl;
                break;
            case 9:
                cout << "nine" << endl;
                break;
            default:
                cout << "ERROR!";
                break;
            }
        }
        else if (i < 1) //sayi 1'den k���k ise hata veriyor
        {
            cout << "ERROR";
            exit(0);
        }
        else //sayi 9'dan buyuk ise bu if condition'� kullaniliyor
        {
            bool e_o;
            e_o = i % 2; //sayi'nin mod2'si aliniyor
            if (e_o) // mod2 degeri 1 ise sayi tektir(odd)
            {
                cout << "odd" << endl;
            }
            else // mod2 degeri 0 ise sayi cifttir(even)
            {
                cout << "even" << endl;
            }
        }
    }



    return 0;
}