#include <iostream>
#include <sstream>
using namespace std;

class Student {
public:
    //class icindeki degiskenler
    int Student_Age;
    int Student_Standard;
    string Student_First_Name;
    string Student_Last_Name;

    void set_age(int age) {
        Student_Age = age; //main'de alinan age degeri class'daki Student_Age degiskenine atiliyor
    };
    void set_first_name(string first_name) {
        Student_First_Name = first_name; //main'de alinan first_name degeri class'daki Student_First_Name degiskenine atiliyor
    };
    void set_last_name(string last_name) {
        Student_Last_Name = last_name; //main'de alinan last_name degeri class'daki Student_Last_Name degiskenine atiliyor
    };
    void set_standard(int standard) {
        Student_Standard = standard; //main'de alinan standard degeri class'daki Student_Standard degiskenine atiliyor
    };
    void to_string() {
        //degerler yan yana yaziliyor
        cout << Student_Age << "," << Student_First_Name << "," << Student_Last_Name << "," << Student_Standard;
    };
};

int main() {
    Student st; //st nesnesi olusturuluyor
    //degiskenler
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard; //degerler kullanicidan aliniyor

    //her bir degisken ilgili olan class fonksiyonuna gonderiliyor
    st.set_age(age);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    st.set_standard(standard);
    
    //class icindeki degiskenlerind degerleri ekrana yaz�l�yor
    cout << st.Student_Age << endl;
    cout << st.Student_Last_Name << ", " << st.Student_First_Name << endl;
    cout << st.Student_Standard << endl << endl;
    st.to_string(); //degerlerin yan yana yazilmasi icin class fonksiyonu kullaniliyor
    

    return 0;
}