#include <iostream>
#include <cstdio>
#include <iomanip>

using namespace std;

int main() {

    //degiskenler
    int a;
    long long b;
    char c;
    float d;
    double e;

    cin >> a >> b >> c >> d >> e; //degiskenler kullanıcıdan alınıyor.

    // degiskenler ekrana bastiriliyor.
    cout << a << endl; 
    cout << b << endl;
    cout << c << endl;
    cout << setprecision(3) << fixed << d << endl; //setprecision ve fixed ile yazim formatlari duzeltiliyor.
    cout << setprecision(9) << fixed << e << endl; //setprecision ve fixed ile yazim formatlari duzeltiliyor.

    return 0;
}