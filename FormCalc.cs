﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_Lab01
{
    public partial class windowCalc : Form
    {
        public windowCalc()
        {

            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            btnColor();
        }
        double result = 0;
        private bool controlIsTextEmpty(string first, string second)
        {
            if (first == "" || second == "")
            {
                MessageBox.Show("Enter numbers.");
                return true;
            }
            return false;
        }

        private void lblColor()
        {
            lblResult.BackColor = result > 0 ? Color.Green : Color.Red;         
        }       
        private void btnColor()
        {
            btnAddition.BackColor = Color.White;
            btnDivision.BackColor = Color.White;
            btnMultiplication.BackColor = Color.White;
            btnSubtraction.BackColor = Color.White;
        }
        private void btnAddition_Click(object sender, EventArgs e)
        {
            btnColor();
            btnAddition.BackColor = Color.Yellow;
            if (controlIsTextEmpty(textBox_Num1.Text, textBox_Num2.Text)) { return; }
            sembol.Text = "+";
            result = Calculator.addition(textBox_Num1.Text, textBox_Num2.Text);
            lblColor();
            lblResult.Text = Math.Round(result, 4).ToString();
        }

        private void btnSubtraction_Click(object sender, EventArgs e)
        {
            btnColor();
            btnSubtraction.BackColor = Color.Yellow;
            if (controlIsTextEmpty(textBox_Num1.Text, textBox_Num2.Text)) { return; }
            sembol.Text = "-";
            result = Calculator.Subtraction(textBox_Num1.Text, textBox_Num2.Text);
            lblColor();
            lblResult.Text = Math.Round(result, 4).ToString();
        }

        private void btnMultiplication_Click(object sender, EventArgs e)
        {
            btnColor();
            btnMultiplication.BackColor = Color.Yellow;
            if (controlIsTextEmpty(textBox_Num1.Text, textBox_Num2.Text)) { return; }
            sembol.Text = "x";
            result = Calculator.multiplication(textBox_Num1.Text, textBox_Num2.Text);
            lblColor();
            lblResult.Text = Math.Round(result, 4).ToString();
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            btnColor();
            btnDivision.BackColor = Color.Yellow;
            if (controlIsTextEmpty(textBox_Num1.Text, textBox_Num2.Text)) { return; }
            sembol.Text = "/";
            if (Convert.ToDouble(textBox_Num2.Text) == 0) 
            {
                lblResult.Text = "ERROR!";
                lblResult.BackColor = Color.DarkRed;
                MessageBox.Show("You can not divided to 0. Enter another number or choose other operations.", "Dikkat!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            result = Calculator.division(textBox_Num1.Text, textBox_Num2.Text);
            lblColor();
            lblResult.Text = Math.Round(result, 4).ToString();
        }

        private void btnAddition_MouseHover(object sender, EventArgs e)
        {
            btnAddition.ForeColor = Color.Red;
        }

        private void btnSubtraction_MouseHover(object sender, EventArgs e)
        {
            btnSubtraction.ForeColor = Color.Red;
        }

        private void btnMultiplication_MouseHover(object sender, EventArgs e)
        {
            btnMultiplication.ForeColor = Color.Red;
        }

        private void btnDivision_MouseHover(object sender, EventArgs e)
        {
            btnDivision.ForeColor = Color.Red;
        }

        private void btnAddition_MouseLeave(object sender, EventArgs e)
        {
            btnAddition.ForeColor = Color.Black;
        }

        private void btnSubtraction_MouseLeave(object sender, EventArgs e)
        {
            btnSubtraction.ForeColor = Color.Black;
        }

        private void btnMultiplication_MouseLeave(object sender, EventArgs e)
        {
            btnMultiplication.ForeColor = Color.Black;
        }

        private void btnDivision_MouseLeave(object sender, EventArgs e)
        {
            btnDivision.ForeColor = Color.Black;
        }

        private void textBox_Num1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter||e.KeyCode==Keys.Right)
            {
                this.ActiveControl = textBox_Num2;
            }
        }

        private void textBox_Num1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }
            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }

        private void textBox_Num2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }
            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }

        private void textBox_Num2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (controlIsTextEmpty(textBox_Num1.Text, textBox_Num2.Text)) { return; }
                MessageBox.Show("Choose operation.", "Dikkat!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (e.KeyCode == Keys.Left)
            {
                this.ActiveControl = textBox_Num1;
            }
        }

        private void ExitButtonCalc_Click(object sender, EventArgs e)
        {
            this.Hide();
        }       
      
    }
}
