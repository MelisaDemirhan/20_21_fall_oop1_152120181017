#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int x, y;
    cin >> x >> y;

    int** A = new int* [x]; //alinan x boyutunda A matrisi dinamik olarak olusturuluyor

    for (int i = 0; i < x; i++)
    {
        int a;
        cin >> a;
        int* B = new int[a]; //alinan a boyutunda B arrayi dinamik olarak olusturuluyor

        for (int j = 0; j < a; j++)
        {
            //B arrayinin elemanlar� aliniyor
            int c;
            cin >> c;
            B[j] = c;
        }
        *(A + i) = B; //A matrisinin bir satiri B arrayini tutuyor
    }

    for (int i = 0; i < y; i++) 
    {
        //A matrisinin istenilen elemanlar� bastiriliyor
        //d kacinci B arrayi/kacinci satir oldugunu. kac�nc� B arrayindeki/satirdaki kacinci eleman oldugu
        int d, e;
        cin >> d >> e;
        cout << A[d][e] << endl;
    }

    return 0;
}