#include <iostream>
#include <cstdio>
using namespace std;

int max_of_four(int, int, int, int); //fonksiyonun tanimlamasi yapiliyor

int main() {

    int a, b, c, d; //degiskenler
    cin >> a >> b >> c >> d; //degiskenler kullanicidan aliniyor
    int max = max_of_four(a, b, c, d); //degiskenler max_of_four fonksiyonuna gidiyor
    cout << max; // max degeri ekrana bastiriliyor

    return 0;
}
int max_of_four(int a, int b, int c, int d) {
    int max;

    //hangi sayinin en buyuk oldugu tespit edilip max degiskenine atiliyor
    max = a > b ? a : b;
    max = max > c ? max : c;
    max = max > d ? max : d;

    return max; //max return ediliyor
}