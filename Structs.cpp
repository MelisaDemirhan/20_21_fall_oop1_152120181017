#include <cmath>
#include <cstdio>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

struct Student {
    // degiskenleri struct Student'�n icinde tutuyoruz
    int age;
    string first_name;
    string last_name;
    int standard;
};

int main() {
    Student st; // st nesnesi olusturuluyor

    cin >> st.age >> st.first_name >> st.last_name >> st.standard; //degerler kullanicidan aliniyor
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard; 

    return 0;
}