#include <iostream>
#include <string>
using namespace std;

int main() {
    string str1, str2, strMix, temp;
    int str1boyutu, str2boyutu;

    cin >> str1;
    cin >> str2;

    str1boyutu = str1.length(); //str1'in uzunlugu hesaplaniyor
    str2boyutu = str2.length(); //str2'nin uzunlugu hesaplaniyor
    strMix = str1 + str2; //str1 ve str2 birlestirilip tek string yapiliyor

    //temp adindaki bos stringin yardimi ile str1 ve str2'nin ilk elemanlari degistiriliyor
    temp[0] = str1[0];
    str1[0] = str2[0];
    str2[0] = temp[0];

    cout << str1boyutu << " " << str2boyutu << endl;
    cout << strMix << endl;
    cout << str1 << " " << str2;

    return 0;
}